#include <bitset>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <forward_list>
#include <iostream>
#include <math.h>
#include <yaml-cpp/yaml.h>

#include <TChain.h>
#include <TVector2.h>

#include <MetFilter.h>
#include <NFRegion.h>
#include <Tree.h>


namespace po = boost::program_options;



int main(int argc, char *argv[]) {
  unsigned int step;
  int event_i = 0, maxEvents;
  std::string output, dataset, year, oldAnlz, verbosity;
  bool isVerbose, crossCheckPlots, passRecOption, isOldAnlz;
  std::vector<std::string> inputs;
  YAML::Node filtersConfig = YAML::LoadFile("config/met_filters.yaml");

  // Managing program options.
  po::options_description optionsDescription{"Options"};
  optionsDescription.add_options()
    ("help,h", "Help screen")
    ("year", po::value<std::string>(&year)->required(), "Dataset year")
    ("output,o", po::value<std::string>(&output)->required(),
     "Output root file")
    ("dataset", po::value<std::string>(&dataset)->required(), "Dataset name")
    ("pass-rec", po::value<bool>(&passRecOption)->default_value(true), "Require"
     "events to pass the recommended MET filters except the one under study")
    ("max-events", po::value<int>(&maxEvents)->default_value(-1),
     "Maximum number of events to process.")
    ("step", po::value<unsigned int>(&step)->default_value(100000),
     "Progress steps (if --verbose[-v] is passed to the program)")
    ("cross-check", po::value<bool>(&crossCheckPlots)->default_value(false),
     "Plot cross-check histograms")
    ("old-analysis", "Enables the analysis without control regions Cuts.")
    ("verbose,v", "Show progress");

  po::options_description hiddenOptionsDescription;
  hiddenOptionsDescription.add_options()
    ("input-files", po::value<std::vector<std::string>>(), "");
  po::positional_options_description posOptionsDescription;
  posOptionsDescription.add("input-files", -1);

  po::options_description allOptionsDescription;
  allOptionsDescription.add(optionsDescription).add(hiddenOptionsDescription);

  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(allOptionsDescription)
      .positional(posOptionsDescription).run(),
      options);

  if (options.count("help")) {
    std::cerr << "Usage:" << std::endl
      << " runAnalysis INPUT_FILE1 [INPUT_FILE2 [...]] [OPTIONS]" << std::endl;
    std::cerr << optionsDescription << std::endl;
    return EXIT_SUCCESS;
  }

  isOldAnlz = options.count("old-analysis");
  isVerbose = options.count("verbose");
  po::notify(options);

  // If no input file is provided throw an exception.
  if (options["input-files"].empty()) {
    std::ostringstream message;
    message << "No input file is provided. Please pass them as POSITIONAL "
      << "arguments.";
    throw std::runtime_error(message.str());
  }

  inputs = options["input-files"].as<std::vector<std::string>>();
  if( maxEvents == -1)
    maxEvents = INT_MAX;

  TString datasetYear = dataset + "_" + year;
  boost::algorithm::to_lower(dataset);

  TFile outputRootFile(output.c_str(), "RECREATE");
  // Creating histograms
  TH1F genWeightSum("genWeightSum", "genWeightSum", 1, 0, 2);
  TH1F metDistrCln("cleanedMetDistr", "cleanedMetDistr", 3700, 300, 4000);
  TH1F metDistrUnCln("uncleanedMetDistr", "uncleanedMetDistr", 3700, 300, 4000);
  TH1F puppimetDistrCln("cleanedPuppiMetDistr", "cleanedPuppiMetDistr", 3700, 300, 4000);
  TH1F puppimetDistrUnCln("uncleanedPuppiMetDistr", "uncleanedPuppiMetDistr", 3700, 300, 4000);
  TH1F ljPhiDistrCln("cleanedLjPhiDistr", "cleanedLjPhiDistr", 628, -3.1416, 3.1416);
  TH1F ljPhiDistrUnCln("uncleanedLjPhiDistr", "uncleanedLjPhiDistr", 628, -3.1416, 3.1416);
  TH1F htDistr("htDistr", "htDistr", 3800, 200, 4000);
  genWeightSum.SetDirectory(&outputRootFile);
  metDistrCln.SetDirectory(&outputRootFile);
  metDistrUnCln.SetDirectory(&outputRootFile);

  TChain chain("ntuplemakerminiaod/tree");
  for (auto const &file : inputs)
    chain.Add(file.c_str());

  if (isVerbose)
    std::cout << "Input files are added into the chain." << std::endl;

  TTreeReader reader(&chain);
  Tree tree(reader);

  NFRegion nfr(reader, dataset, year, crossCheckPlots);
  auto run = tree.run;
  //auto lumi = tree.lumi;
  auto event = tree.event;
  auto npvx = tree.npvx;
  auto pfmet = tree.pfmet;
  auto puppimet = tree.puppimet;
  auto jetPt = tree.jetPt;
  auto jetEta = tree.jetEta;
  auto jetPhi = tree.jetPhi;
  auto ht_3To5 = tree.ht_3To5;
  auto genWeight = tree.genWeight;
  auto pfmetTrigger110 = tree.pfmetTrigger110;
  auto pfmetTrigger120 = tree.pfmetTrigger120;
  auto pfmetTrigger130 = tree.pfmetTrigger130;
  auto pfmetTrigger140 = tree.pfmetTrigger140;
  auto ht = tree.ht;
  auto numJetsPt25 = tree.numJetsPt25;
  auto pfmetPhi = tree.pfmetPhi;

  std::vector<MetFilter> metFilters;
  for (auto const &filter : filtersConfig) {
   metFilters.emplace_back(
       datasetYear, reader,
       filter["name"].as<std::string>(),
       filter["flag"].as<std::string>(),
       filter["isRecommended"].as<bool>(),
       filter["N-1key"].as<int>(),
       isOldAnlz);
  }

  for (auto &f : metFilters) {
    f.SetDirectory(outputRootFile);
  }

  // Main loop.
  std::cout << chain.GetEntries() << "\n";
  float genWeightSumVal = 0, htCut = 500;
  while(reader.Next()) {
    event_i++;
    if (event_i > maxEvents)
      break;

    if (isVerbose and (event_i % step == 0))
      std::cout << event_i << " events are processed" << std::endl;

    genWeightSumVal += *genWeight;
    if ((*pfmet < 300 and *puppimet < 300) or (*jetPt).size() < 2)
      continue;

    int jetNum = 0, zero = 0;
    float myHt = 0;
    // Computing ht and checking for number of good jets
    for (unsigned int i = 0; i < (*jetPt).size(); i++) {
        jetNum++;
        if (jetNum == 1)
          zero = i;
        if (std::fabs((*jetEta)[i]) < 2.4) {
          myHt += (*jetPt)[i];
        }
    }

    if (myHt < htCut)
      continue;

    if (jetNum < 2)
      continue;

    bool hemEffected = false;
    // FOR VETOING THE HEM REGION FOR ERA C AND D IN 2018, UNCOMMENT BELOW LINES:
    //if (year == "2018" and dataset != "mc" and *run >= 319077) {
    //  for (unsigned int i = 0; i < (*jetPt).size(); i++) {
    //    if ((*jetPt)[i] < 30)
    //      continue;
    //    if ((*jetEta)[i] >= -3.20 and (*jetEta)[i] <= -1.2 and
    //        (*jetPhi)[i] >= -1.77 && (*jetPhi)[i] <= -0.67 and
    //        (std::fabs(TVector2::Phi_mpi_pi(*pfmetPhi - (*jetPhi)[i])) < 0.2)) {
    //      hemEffected = true;
    //      break;
    //    }
    //  }
    //}
    //float ptThreshold = 30;
    //if (year == "2018" and dataset == "mc") {
    //  if ((*event % 1000) > 1000 * 21.0 / 59.6) {
    //    for (unsigned int i = 0; i < (*jetPt).size(); i++) {
    //      if (-3.2 <= (*jetEta)[i] and (*jetEta)[i] <= -1.2 and
    //          -1.77 <= (*jetPhi)[i] and (*jetPhi)[i] <= -0.67 and
    //          (*jetPt)[i] > ptThreshold and
    //          std::fabs(TVector2::Phi_mpi_pi(*pfmetPhi - (*jetPhi)[i])) < 0.2) {
    //        hemEffected = true;
    //      }
    //    }
    //  }
    //}


    // Filling histograms for uncleand data or MC
    if (*pfmet >= 300 and not hemEffected) {
      metDistrUnCln.Fill(*pfmet, *genWeight);
      ljPhiDistrUnCln.Fill((*jetPhi)[zero], *genWeight);
    }
    if (*puppimet >= 300 and not hemEffected)
      puppimetDistrUnCln.Fill(*puppimet, *genWeight);

    // Redoing above analysis for cleaned data or MC
    jetNum = 0; zero = 0; myHt = 0;
    for (unsigned int i = 0; i < (*jetPt).size(); i++) {
      if (nfr.passJetId_2018(i)) {
        jetNum++;
        if (jetNum == 1)
          zero = i;
        if (std::fabs((*jetEta)[i]) < 2.4) {
          myHt += (*jetPt)[i];
        }
      }
    }

    if (myHt < htCut)
      continue;

    if (jetNum < 2)
      continue;

    // FOR VETOING THE HEM REGION FOR ERA C AND D IN 2018, UNCOMMENT BELOW LINES:
    //hemEffected = false;
    //if (year == "2018" and dataset != "mc" and *run >= 319077) {
    //  for (unsigned int i = 0; i < (*jetPt).size(); i++) {
    //    if ((*jetPt)[i] < 30)
    //      continue;
    //    if (not nfr.passJetId_2018(i))
    //      continue;
    //    if ((*jetEta)[i] >= -3.20 and (*jetEta)[i] <= -1.2 and
    //        (*jetPhi)[i] >= -1.77 && (*jetPhi)[i] <= -0.67 and
    //        (std::fabs(TVector2::Phi_mpi_pi(*pfmetPhi - (*jetPhi)[i])) < 0.2)) {
    //      hemEffected = true;
    //      break;
    //    }
    //  }
    //}
    //ptThreshold = 30;
    //if (year == "2018" and dataset == "mc") {
    //  if ((*event % 1000) > 1000 * 21.0 / 59.6) {
    //    for (unsigned int i = 0; i < (*jetPt).size(); i++) {
    //      if (-3.2 <= (*jetEta)[i] and (*jetEta)[i] <= -1.2 and
    //          -1.77 <= (*jetPhi)[i] and (*jetPhi)[i] <= -0.67 and
    //          (*jetPt)[i] > ptThreshold and
    //          std::fabs(TVector2::Phi_mpi_pi(*pfmetPhi - (*jetPhi)[i])) < 0.2) {
    //        hemEffected = true;
    //      }
    //    }
    //  }
    //}
    //if (hemEffected)
    //  continue;

    if (dataset != "mc")
      if (not(*pfmetTrigger110 or *pfmetTrigger120 or *pfmetTrigger130
            or *pfmetTrigger140))
        continue;

    // Applying recommended MET filters
    // Please modify the met filters config file according to the
    // recommendation on filters according to whether you are dealing with
    // data or MC, UL or EOY, and 2016 or 2017 or 2018.
    bool passRecs = true;
    for (auto &metFilter : metFilters) {
      if (not metFilter.IsRecommeded())
        continue;
      passRecs *= metFilter.IsPassed();
    }

    // Filling histograms for cleanded data or MC
    if (passRecs) {
      if (*pfmet >= 300) {
        metDistrCln.Fill(*pfmet, *genWeight);
        ljPhiDistrCln.Fill((*jetPhi)[zero], *genWeight);
      }
      if (*puppimet >= 300)
        puppimetDistrCln.Fill(*puppimet, *genWeight);
    }

  } // End of main loop.

  genWeightSum.Fill(1, genWeightSumVal);
  outputRootFile.cd();
  outputRootFile.Write();
  outputRootFile.Close();
  if (isVerbose)
    std::cout << event_i << " events are processed." << std::endl;

  return 0;
}

