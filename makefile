COMPILEPATH = $(PWD)
INCLUDEDIR = $(COMPILEPATH)/include
SRCDIR = $(COMPILEPATH)/src
BINDIR = $(COMPILEPATH)/bin
SOURCES += $(SRCDIR)/MetFilter.cc
SOURCES += $(SRCDIR)/NFRegion.cc
SOURCES += $(SRCDIR)/Tree.cc
runAnalysis = runAnalysis
finalHistograms = finalHistograms

CXX         = g++
CXXFLAGS    = -g -O3 -Wall
CXXFLAGS   += -I$(INCLUDEDIR)
CXXFLAGS   += -lboost_program_options
CXXFLAGS   += -lyaml-cpp
CXXFLAGS   += `root-config --cflags --glibs`

.PHONY: all clean


all: $(finalHistograms)

$(finalHistograms): $(SRCDIR)/$(finalHistograms).cc
	$(CXX) $(SOURCES) $(SRCDIR)/$(finalHistograms).cc -L$(MAIN_LIB_DIR) -std=c++17 -lstdc++fs $(CXXFLAGS) -o $(BINDIR)/$(finalHistograms)


SOURCES += $(SRCDIR)/BERegion.cc
all: $(runAnalysis)

$(runAnalysis): $(SRCDIR)/$(runAnalysis).cc
	$(CXX) $(SOURCES) $(SRCDIR)/$(runAnalysis).cc -L$(MAIN_LIB_DIR) -std=c++17 -lstdc++fs $(CXXFLAGS) -o $(BINDIR)/$(runAnalysis)


clean:
	rm -f $(SRCDIR)*.o
	rm -f $(SRCDIR)*.so
	rm -f $(SRCDIR)*.d
	rm -f $(BINDIR)*.o
	rm -f $(BINDIR)*.so
	rm -f $(BINDIR)*.d
	rm -f $(BINDIR)/$(runAnalysis)
	rm -f $(BINDIR)/$(finalHistograms)
