# Consistent LCG environment (http://lcginfo.cern.ch)
release=`cat /etc/redhat-release`
if test "${release#*release 6.}" != "$release"; then
  # Scientific Linux 6
  . /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-slc6-gcc8-opt
  export MAIN_LIB_DIR='/cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-slc6-gcc8-opt/lib/'
else
  # CentOS 7
  . /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_97python3 x86_64-centos7-gcc9-opt
  export MAIN_LIB_DIR='/cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/lib'
fi

export PATH=$PATH:$(pwd)/bin
